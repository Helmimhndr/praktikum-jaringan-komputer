﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpClient
{
    public static void Main()
    {
        byte[] data = new byte[1024];
        int recv;
        string input, stringData, uname;
        IPEndPoint ipep = new IPEndPoint(
                        IPAddress.Parse("192.168.43.168"), 9050);

        Socket server = new Socket(AddressFamily.InterNetwork,
                       SocketType.Dgram, ProtocolType.Udp);


        Console.Write("Type your uname : ");
        string welcome = Console.ReadLine();
        data = Encoding.ASCII.GetBytes(welcome);
        server.SendTo(data, data.Length, SocketFlags.None, ipep);

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)sender;

        recv = server.ReceiveFrom(data, ref Remote);
        uname = Encoding.ASCII.GetString(data, 0, recv);
        Console.WriteLine("Server Uname : " + uname);


        while (true)
        {
            Console.Write(welcome + " : ");
            input = Console.ReadLine();
            if (input == "exit")
                break;
            server.SendTo(Encoding.ASCII.GetBytes(input), Remote);
            data = new byte[1024];
            recv = server.ReceiveFrom(data, ref Remote);
            stringData = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine(uname + " : " + stringData);

        }
        Console.WriteLine("Stopping client");
        server.Close();
    }
}